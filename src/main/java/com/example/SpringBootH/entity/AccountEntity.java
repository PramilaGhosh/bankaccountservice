package com.example.SpringBootH.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.persistence.*;
@Component
@Getter
@Setter
@Entity(name="accountdetails")
public class AccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String accNo;
    @Column
    private String type;
    @Column
    private String bankName;
}
