package com.example.SpringBootH.web.controllers;

import com.example.SpringBootH.entity.AccountEntity;
import com.example.SpringBootH.service.BankAccountService;
import com.example.SpringBootH.validator.AccountDetailsValidator;
import com.example.SpringBootH.web.module.AccountCreateRequest;
import com.example.SpringBootH.web.module.AccountDetailsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class BankAccountController {

    @Autowired
    private AccountEntity accountEntity;
    @Autowired
    private BankAccountService bankAccountService;
    @Autowired
    AccountDetailsValidator accountDetailsValidator;

    @PostMapping(path = "/add/acc")
    public Object createContract(@RequestBody AccountCreateRequest accountDetails) throws RuntimeException {
        accountEntity.setAccNo(accountDetails.getAccNo());
        accountEntity.setBankName(accountDetails.getBankName());
        accountEntity.setType(accountDetails.getAccType());
        return bankAccountService.createAccount(accountEntity);

    }
    @GetMapping(path = "/get/acc")
    public Object getAccountDetails(@RequestBody AccountDetailsRequest accountInfo){
        return bankAccountService.getAccountDetail(accountInfo);
    }




}
