package com.example.SpringBootH.web.module;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class AccountCreateResponse {
    private String accNo;
    private String accType;
    private String bankName;


}
