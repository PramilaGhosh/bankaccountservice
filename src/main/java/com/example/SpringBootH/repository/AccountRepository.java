package com.example.SpringBootH.repository;

import com.example.SpringBootH.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends PagingAndSortingRepository<AccountEntity,Long> {
    Optional<AccountEntity> findByAccNo(String accNo);

}
