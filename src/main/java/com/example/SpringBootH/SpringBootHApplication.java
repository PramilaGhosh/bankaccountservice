package com.example.SpringBootH;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHApplication.class, args);
	}

}
