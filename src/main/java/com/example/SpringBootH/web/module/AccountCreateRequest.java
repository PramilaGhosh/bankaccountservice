package com.example.SpringBootH.web.module;

//import lombok.*;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Data // set data using setter method
@Component
public class AccountCreateRequest {
private String accNo;
private String accType;
private String bankName;

}
