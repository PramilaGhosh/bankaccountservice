package com.example.SpringBootH.service;

import com.example.SpringBootH.entity.AccountEntity;
import com.example.SpringBootH.repository.AccountRepository;
import com.example.SpringBootH.validator.AccountDetailsValidator;
import com.example.SpringBootH.web.module.AccountCreateResponse;
import com.example.SpringBootH.web.module.AccountDetailsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BankAccountService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AccountCreateResponse response;
    @Autowired
    AccountDetailsValidator accountDetailsValidator;
    public Object createAccount(AccountEntity accountInfo)throws RuntimeException{
        accountDetailsValidator.validateCreateAccountDetails(accountInfo);
        AccountEntity responseEntity= accountRepository.save(accountInfo);
        response.setAccNo(responseEntity.getAccNo());
        response.setAccType(responseEntity.getType());
        response.setBankName(responseEntity.getBankName());
        return response;
    }

    public Object getAccountDetail(AccountDetailsRequest accountInfo) {
        if(accountInfo.getAccNo()=="") return accountRepository.findAll();
        Optional<AccountEntity> accountDetails=accountRepository.findByAccNo(accountInfo.getAccNo());
        if(!accountDetails.isPresent()){
           return "No account has created by this account no";

        }
        return accountDetails;

    }
}
