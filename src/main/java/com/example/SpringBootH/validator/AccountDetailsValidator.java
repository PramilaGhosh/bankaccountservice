package com.example.SpringBootH.validator;

import com.example.SpringBootH.entity.AccountEntity;
import com.example.SpringBootH.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AccountDetailsValidator {
    @Autowired
    AccountRepository accountRepository;



    public void validateCreateAccountDetails(AccountEntity accountInfo) {
        Optional<AccountEntity> opt = accountRepository.findByAccNo(accountInfo.getAccNo());

        if (opt.isPresent()) {
            log.error("Account already got created");
            throw new RuntimeException("Account already got created");

        }
    }
}
